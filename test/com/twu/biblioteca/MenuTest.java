package com.twu.biblioteca;

import com.twu.biblioteca.handler.InvalidOptionHandler;
import com.twu.biblioteca.handler.ListAvailableBooksHanlder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.twu.biblioteca.IO.FAILED_LOGIN_MSG;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(IO.class)
public class MenuTest {

    @Test
    public void should_map_option_to_the_correct_handler() {
        PowerMockito.mockStatic(IO.class);
        ListAvailableBooksHanlder listAvailableBooksHanlder = mock(ListAvailableBooksHanlder.class);
        InvalidOptionHandler defaultHandler = mock(InvalidOptionHandler.class);
        Customer customer = mock(Customer.class);
        Menu menu = new Menu(customer);
        menu.setHandlerMap(Stream.of(
                new SimpleEntry<>("1", listAvailableBooksHanlder),
                new SimpleEntry<>("default", defaultHandler)
        ).collect(Collectors.toMap(Entry::getKey, Entry::getValue)));

        when(customer.isLoggedIn()).thenReturn(true);
        when(customer.getUsername()).thenReturn("Jan");
        when(IO.readInput()).thenReturn("1");
        menu.entranceMenu();
        verify(listAvailableBooksHanlder, times(1)).handle(customer);

        when(IO.readInput()).thenReturn("998");
        menu.entranceMenu();
        verify(defaultHandler, times(1)).handle(customer);
    }

    @Test
    public void should_return_warning_message_when_login_fails() {
        PowerMockito.mockStatic(IO.class);
        Customer customer = mock(Customer.class);
        Menu menu = new Menu(customer);

        when(customer.isLoggedIn()).thenReturn(false);
        when(IO.readInput()).thenReturn("username", "password");
        Assert.assertEquals(FAILED_LOGIN_MSG, menu.entranceMenu());
    }
}