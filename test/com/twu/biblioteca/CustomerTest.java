package com.twu.biblioteca;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class CustomerTest {

    @Test
    public void customer_can_checkout_an_available_book() {
        Customer customer = new Customer("Jan");
        Book book = new Book("test book", Collections.singletonList(new Author("test author")), " 2019", "Biblioteca");
        customer.borrowBook(book);
        assertFalse(book.owner().isEmpty());
    }

    @Test
    public void customer_can_checkin_a_book() {
        Book book = new Book("test book", Collections.singletonList(new Author("test author")), " 2019", "Biblioteca");
        Customer customer = new Customer("Jan", Collections.singletonList(book), Arrays.asList());
        customer.returnBook(book);
        assertTrue(book.owner().isEmpty());
    }

    @Test
    public void customer_can_checkout_an_available_moview() {
        Customer customer = new Customer("Jan");
        Movie movie = new Movie("The Godfather","1972","test author");
        customer.borrowMovie(movie);
        assertFalse(movie.owner().isEmpty());
    }

    @Test
    public void customer_can_login_with_right_credential() {
        Customer customer = new Customer();
        assertFalse(customer.isLoggedIn());
        customer.login("Jan", "123456");
        assertTrue(customer.isLoggedIn());
    }

    @Test
    public void customer_cannot_login_with_wrong_credential() {
        Customer customer = new Customer();
        customer.login("", "");
        assertFalse(customer.isLoggedIn());
    }
}