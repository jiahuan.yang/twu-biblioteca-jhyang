package com.twu.biblioteca;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BibliotecaTest {

    private List<String> books = new ArrayList<>();
    private List<String> movies = new ArrayList<>();
    private Biblioteca biblioteca;

    @Before
    public void before() {
        biblioteca = Biblioteca.getBiblioteca();
        books.add("Refactoring - [Martin Fowler] - 1999");
        books.add("Enterprise Agility: Being Agile in a Changing World - [Sunil Mnudra] - 2018");
        books.add("Building Evolutionary Architectures - [Neal Ford, Rebecca Parsons, Patrick Kua] - 2017");

        movies.add("The Godfather - Francis Ford Coppola - 1972");
        movies.add("The Dark Knight - Christopher Nolan - 2008");
        movies.add("The Lord of the Rings: The Return of the King - Peter Jackson - 2003");
    }

    @After
    public void after() {
        biblioteca.destory();
    }

    @Test
    public void should_view_all_books_with_title_author_and_publication_year() {
        assertEquals(books, biblioteca.getAllBooksAsString());
    }

    @Test
    public void should_view_all_available_books() {
        assertEquals(books, biblioteca.getAvailableBooksAsString());
    }

    @Test
    public void should_view_all_available_movies() {
        assertEquals(movies, biblioteca.getAllMoviesAsString());
    }
}