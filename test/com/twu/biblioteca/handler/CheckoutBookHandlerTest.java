package com.twu.biblioteca.handler;

import com.twu.biblioteca.Biblioteca;
import com.twu.biblioteca.Customer;
import com.twu.biblioteca.IO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.twu.biblioteca.IO.BOOK_NOT_AVAILABLE_MSG;
import static com.twu.biblioteca.IO.CHECKOUT_BOOK_MSG;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(IO.class)
public class CheckoutBookHandlerTest {
    private Biblioteca biblioteca;
    private Customer customer = new Customer("Jan");
    private CheckoutBookHandler checkoutBookHandler = new CheckoutBookHandler();

    @Before
    public void before() {
        PowerMockito.mockStatic(IO.class);
        biblioteca = Biblioteca.getBiblioteca();
    }

    @After
    public void after() {
        biblioteca.destory();
    }

    @Test
    public void should_checkout_an_available_book_successfully() {
        when(IO.readInput()).thenReturn("0");
        assertEquals(CHECKOUT_BOOK_MSG, checkoutBookHandler.handle(customer));
    }

    @Test
    public void should_not_checkout_an_unavailable_book() {
        when(IO.readInput()).thenReturn("9");
        assertEquals(BOOK_NOT_AVAILABLE_MSG, checkoutBookHandler.handle(customer));
    }
}