package com.twu.biblioteca.handler;

import com.twu.biblioteca.Customer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ListAvailableBooksHanlderTest {
    private Customer customer = new Customer("Jan");
    private ListAvailableBooksHanlder listAvailableBooksHanlder = new ListAvailableBooksHanlder();
    private static final String ALL_BOOKS = "0. Refactoring - [Martin Fowler] - 1999\n" +
            "1. Enterprise Agility: Being Agile in a Changing World - [Sunil Mnudra] - 2018\n" +
            "2. Building Evolutionary Architectures - [Neal Ford, Rebecca Parsons, Patrick Kua] - 2017";

    @Test
    public void shoud_list_all_available_books() {
        assertEquals(listAvailableBooksHanlder.handle(customer), ALL_BOOKS);
    }
}