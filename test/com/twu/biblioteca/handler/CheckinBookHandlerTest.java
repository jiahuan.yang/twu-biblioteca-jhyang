package com.twu.biblioteca.handler;

import com.twu.biblioteca.Author;
import com.twu.biblioteca.Book;
import com.twu.biblioteca.Customer;
import com.twu.biblioteca.IO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Collections;

import static com.twu.biblioteca.IO.CHECKIN_BOOK_MSG;
import static com.twu.biblioteca.IO.WRONG_BOOK_MSG;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(IO.class)
public class CheckinBookHandlerTest {
    private Book bibliotecaBook = new Book("test book", Collections.singletonList(new Author("test author")), " 2019", "Biblioteca");
    private Book libraryBook = new Book("test book", Collections.singletonList(new Author("test author")), " 2019", "library");
    private CheckinBookHandler checkinBookHandler = new CheckinBookHandler();

    @Before
    public void before() {
        PowerMockito.mockStatic(IO.class);
    }

    @Test
    public void should_checkin_a_borrowed_book_successfully() {
        Customer customer = new Customer("Jan", Collections.singletonList(bibliotecaBook), Arrays.asList());
        when(IO.readInput()).thenReturn("0");
        assertEquals(CHECKIN_BOOK_MSG, checkinBookHandler.handle(customer));
    }

    @Test
    public void should_not_checkin_a_non_borrowed_book() {
        Customer customer = new Customer("Jan");
        when(IO.readInput()).thenReturn("0");
        assertEquals(WRONG_BOOK_MSG, checkinBookHandler.handle(customer));
    }

    @Test
    public void should_not_checkin_a_non_Biblioteca_book() {
        Customer customer = new Customer("Jan", Collections.singletonList(libraryBook), Arrays.asList());
        when(IO.readInput()).thenReturn("0");
        assertEquals(WRONG_BOOK_MSG, checkinBookHandler.handle(customer));
    }
}