package com.twu.biblioteca.handler;

import com.twu.biblioteca.Customer;
import com.twu.biblioteca.IO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.twu.biblioteca.IO.QUIT_MSG;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(IO.class)
public class CheckBibliotecaHandlerTest {

    @Test
    public void should_quit_biblioteca_after_press_any_key() {
        PowerMockito.mockStatic(IO.class);
        Customer customer = mock(Customer.class);
        CheckBibliotecaHandler checkBibliotecaHandler = new CheckBibliotecaHandler();
        when(IO.readInput()).thenReturn("anykey");
        assertEquals(QUIT_MSG, checkBibliotecaHandler.handle(customer));
    }
}