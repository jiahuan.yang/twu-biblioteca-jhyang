package com.twu.biblioteca.handler;

import com.twu.biblioteca.Customer;
import org.junit.Test;

import static com.twu.biblioteca.IO.QUIT_MSG;
import static org.junit.Assert.assertEquals;

public class QuitBibliotecaHandlerTest {
    private Customer customer = new Customer("Jan");
    private QuitBibliotecaHandler quitBibliotecaHandler = new QuitBibliotecaHandler();

    @Test
    public void should_return_invalid_option_message() {
        assertEquals(quitBibliotecaHandler.handle(customer), QUIT_MSG);
    }
}