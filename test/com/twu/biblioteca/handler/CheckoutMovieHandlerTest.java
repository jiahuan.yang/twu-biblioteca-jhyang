package com.twu.biblioteca.handler;

import com.twu.biblioteca.Biblioteca;
import com.twu.biblioteca.Customer;
import com.twu.biblioteca.IO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.twu.biblioteca.IO.CHECKOUT_MOVIE_MSG;
import static com.twu.biblioteca.IO.MOVIE_NOT_AVAILABLE_MSG;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(IO.class)
public class CheckoutMovieHandlerTest {
    private Biblioteca biblioteca;
    private Customer customer = new Customer("Jan");
    private CheckoutMovieHandler checkoutMovieHandler = new CheckoutMovieHandler();

    @Before
    public void before() {
        PowerMockito.mockStatic(IO.class);
        biblioteca = Biblioteca.getBiblioteca();
    }

    @After
    public void after() {
        biblioteca.destory();
    }

    @Test
    public void should_checkout_an_available_movie_successfully() {
        when(IO.readInput()).thenReturn("0");
        assertEquals(CHECKOUT_MOVIE_MSG, checkoutMovieHandler.handle(customer));
    }

    @Test
    public void should_not_checkout_an_unavailable_movie() {
        when(IO.readInput()).thenReturn("9");
        assertEquals(MOVIE_NOT_AVAILABLE_MSG, checkoutMovieHandler.handle(customer));
    }
}