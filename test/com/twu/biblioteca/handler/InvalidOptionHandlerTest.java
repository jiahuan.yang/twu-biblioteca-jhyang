package com.twu.biblioteca.handler;

import com.twu.biblioteca.Customer;
import org.junit.Test;

import static com.twu.biblioteca.handler.InvalidOptionHandler.INVALID_OPTION_MSG;
import static org.junit.Assert.assertEquals;

public class InvalidOptionHandlerTest {
    private Customer customer = new Customer("Jan");
    private InvalidOptionHandler invalidOptionHandler = new InvalidOptionHandler();

    @Test
    public void should_return_invalid_option_message() {
        assertEquals(invalidOptionHandler.handle(customer), INVALID_OPTION_MSG);
    }
}