select name from member
    left join checkout_item on member.id = checkout_item.member_id
    group by checkout_item.member_id
    having count(member_id) > 1;
