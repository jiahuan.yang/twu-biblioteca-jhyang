### Q1:
```
  select name
    from member
    left join checkout_item on member.id = checkout_item.member_id
    left join book on checkout_item.book_id = book.id
    where book.title = "The Hobbit";
```
answer: Anand Beck

### Q2:
```
select sum(1)
    from member
    left join checkout_item on member.id = checkout_item.member_id
    where checkout_item.member_id is NULL;
```
answer: 37

### Q3:
```
select book.title from book left join checkout_item on book.id = checkout_item.book_id where checkout_item.book_id is NULL;
select movie.title from movie left join checkout_item on movie.id = checkout_item.movie_id where checkout_item.movie_id is NULL;
```
answer: 
Fellowship of the Ring
1984
Tom Sawyer
Catcher in the Rye
To Kill a Mockingbird
Domain Driven Design
Thin Red Line
Crouching Tiger, Hidden Dragon
Lawrence of Arabia
Office Space

# Q4:
```
insert into book (title) values ("The Pragmatic Programmer");
insert into member (name) values ("Jiahuan Yang");
insert into checkout_item (member_id, book_id) values (43, 11);
select name
    from member
    left join checkout_item on member.id = checkout_item.member_id i
    left join book on checkout_item.book_id = book.id
    where book.title = "The Pragmatic Programmer";
```
answer: Jiahuan Yang

# Q5:
```
select name from member
    left join checkout_item on member.id = checkout_item.member_id
    group by checkout_item.member_id
    having count(member_id) > 1;
```
answer: 
Anand Beck
Frank Smith