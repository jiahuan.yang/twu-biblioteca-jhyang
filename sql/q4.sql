insert into book (title) values ("The Pragmatic Programmer");
insert into member (name) values ("Jiahuan Yang");
insert into checkout_item (member_id, book_id) values (43, 11);
select name 
    from member
    left join checkout_item on member.id = checkout_item.member_id i
    left join book on checkout_item.book_id = book.id 
    where book.title = "The Pragmatic Programmer";
