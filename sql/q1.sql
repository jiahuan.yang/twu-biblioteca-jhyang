select name
    from member
    left join checkout_item on member.id = checkout_item.member_id
    left join book on checkout_item.book_id = book.id
    where book.title = "The Hobbit";
