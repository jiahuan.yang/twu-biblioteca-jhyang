package com.twu.biblioteca;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String username;
    private String name = "Jan";
    private String email = "jiahuan.yang@thoughtworks.com";
    private String phone = "12345678";
    private List<Book> borrowedBooks = new ArrayList<>();
    private List<Movie> borrowedMovies = new ArrayList<>();
    private boolean isLoggedIn = false;

    public Customer() {
    }

    public Customer(String username) {
        this.username = username;
    }

    public Customer(String username, List<Book> books, List<Movie> movies) {
        this.username = username;
        borrowedBooks.addAll(books);
        borrowedMovies.addAll(movies);
    }

    public void login(String username, String password) {
        if (UserAccount.userCredentialMap.get(username) != null &&
                UserAccount.userCredentialMap.get(username).equals(password)) {
            this.username = username;
            this.isLoggedIn = true;
        }
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public String getUsername() {
        return username;
    }

    public List<Book> getBorrowedBooks() {
        return borrowedBooks;
    }

    public void borrowBook(Book book) {
        //TODO transactional
        this.borrowedBooks.add(book);
        book.setOwner(username);
    }

    public void returnBook(Book book) {
        //TODO transactional
        this.borrowedBooks.remove(book);
        book.setOwner("");
    }

    public void borrowMovie(Movie movie) {
        //TODO transactional
        this.borrowedMovies.add(movie);
        movie.setOwner(username);
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
