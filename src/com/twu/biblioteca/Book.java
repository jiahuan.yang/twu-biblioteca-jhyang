package com.twu.biblioteca;

import java.text.MessageFormat;
import java.util.List;

public class Book implements HasOwner {
    private String title;
    private List<Author> authors;
    private String year;
    private String library;
    private String owner;

    public Book(String title, List<Author> authors, String year, String library) {
        this.title = title;
        this.authors = authors;
        this.year = year;
        this.library = library;
        this.owner = "";
    }

    private String getAuthorsAsString() {
        return authors.toString();
    }

    public String getLibrary() {
        return library;
    }

    @Override
    public String owner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} - {1} - {2}", title, getAuthorsAsString(), year);
    }

}
