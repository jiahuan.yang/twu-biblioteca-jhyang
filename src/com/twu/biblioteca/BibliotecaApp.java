package com.twu.biblioteca;

import static com.twu.biblioteca.IO.QUIT_MSG;

public class BibliotecaApp {

    public static void main(String[] args) {

        System.out.println("Welcome to Biblioteca. Your one-stop-shop for great book titles in Bangalore!");

        Customer customer = new Customer("");
        Menu menu = new Menu(customer);

        String message = menu.entranceMenu();
        System.out.println(message);

        while (!message.equals(QUIT_MSG)) {
            message = menu.entranceMenu();
            System.out.println(message);
        }
    }
}