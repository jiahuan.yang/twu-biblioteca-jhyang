package com.twu.biblioteca;

import com.twu.biblioteca.handler.CheckBibliotecaHandler;
import com.twu.biblioteca.handler.CheckinBookHandler;
import com.twu.biblioteca.handler.CheckoutBookHandler;
import com.twu.biblioteca.handler.CheckoutMovieHandler;
import com.twu.biblioteca.handler.InvalidOptionHandler;
import com.twu.biblioteca.handler.ListAvailableBooksHanlder;
import com.twu.biblioteca.handler.CheckUserInfoHandler;
import com.twu.biblioteca.handler.OptionHandler;
import com.twu.biblioteca.handler.QuitBibliotecaHandler;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.twu.biblioteca.IO.FAILED_LOGIN_MSG;
import static com.twu.biblioteca.IO.SUCCESSFUL_LOGIN_MSG;
import static com.twu.biblioteca.IO.readInput;

public class Menu {
    private Customer customer;
    private Map<String, OptionHandler> handlerMap = Stream.of(
            new SimpleEntry<>("1", new ListAvailableBooksHanlder()),
            new SimpleEntry<>("2", new CheckoutBookHandler()),
            new SimpleEntry<>("3", new CheckinBookHandler()),
            new SimpleEntry<>("4", new CheckoutMovieHandler()),
            new SimpleEntry<>("5", new CheckUserInfoHandler()),
            new SimpleEntry<>("6", new QuitBibliotecaHandler()),
            new SimpleEntry<>("default", new InvalidOptionHandler())
    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    public Menu(Customer customer) {
        this.customer = customer;
    }

    public void setHandlerMap(Map<String, OptionHandler> handlerMap) {
        this.handlerMap = handlerMap;
    }

    public String entranceMenu() {
        if (customer.isLoggedIn()) {
            if (customer.getUsername().equals("admin")) {
                return adminMenu();
            } else return userMenu();
        } else return loginMenu();
    }

    private String adminMenu() {
        System.out.println("Following are all the books in Biblioteca with its owner:");
        return new CheckBibliotecaHandler().handle(customer);
    }

    private String userMenu() {
        System.out.println("Please select from the following options:\n" +
                "1. List all available books\n" +
                "2. Checkout one book\n" +
                "3. Checkin one book\n" +
                "4. Checkout one movie\n" +
                "5. Check user info\n" +
                "6. Exit Biblioteca");
        return handlerMap.getOrDefault(readInput(), handlerMap.get("default")).handle(customer);
    }

    private String loginMenu() {
        System.out.print("Please enter the username and password:\n" + "username: ");
        String username = readInput();
        System.out.print("password: ");
        String password = readInput();
        customer.login(username, password);
        if (customer.isLoggedIn()) {
            return SUCCESSFUL_LOGIN_MSG;
        } else {
            return FAILED_LOGIN_MSG;
        }
    }
}
