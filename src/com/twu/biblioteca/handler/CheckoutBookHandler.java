package com.twu.biblioteca.handler;

import com.twu.biblioteca.Biblioteca;
import com.twu.biblioteca.Book;
import com.twu.biblioteca.Customer;
import com.twu.biblioteca.IO;

import java.util.List;

import static com.twu.biblioteca.IO.BOOK_NOT_AVAILABLE_MSG;
import static com.twu.biblioteca.IO.CHECKOUT_BOOK_MSG;

public class CheckoutBookHandler implements OptionHandler {
    private Biblioteca biblioteca = Biblioteca.getBiblioteca();

    @Override
    public String handle(Customer customer) {
        System.out.println("Which book would you like to check out?");
        List<Book> availableBooks = biblioteca.getAvailableBooks();
        availableBooks.forEach(
                book -> System.out.println(availableBooks.indexOf(book) + ". " + book));

        String bookNumber = IO.readInput();
        if (isValidInput(bookNumber)) {
            return availableBooks.stream()
                    .filter(book -> book.equals(availableBooks.get(Integer.parseInt(bookNumber))))
                    .findFirst()
                    .map(book -> {
                        customer.borrowBook(book);
                        return CHECKOUT_BOOK_MSG;
                    })
                    .orElse(BOOK_NOT_AVAILABLE_MSG);
        } else {
            return BOOK_NOT_AVAILABLE_MSG;
        }
    }

    private boolean isValidInput(String bookNumber) {
        return bookNumber.chars().allMatch(Character::isDigit) &&
                Integer.parseInt(bookNumber) >= 0 &&
                Integer.parseInt(bookNumber) < biblioteca.getAvailableBooks().size();
    }

}
