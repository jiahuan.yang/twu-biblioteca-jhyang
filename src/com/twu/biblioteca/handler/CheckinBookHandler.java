package com.twu.biblioteca.handler;

import com.twu.biblioteca.Book;
import com.twu.biblioteca.Customer;
import com.twu.biblioteca.IO;

import java.util.List;

import static com.twu.biblioteca.IO.CHECKIN_BOOK_MSG;
import static com.twu.biblioteca.IO.WRONG_BOOK_MSG;

public class CheckinBookHandler implements OptionHandler {
    private Customer customer;

    @Override
    public String handle(Customer customer) {
        this.customer = customer;
        System.out.println("Which book would you like to check in?");
        List<Book> borrowedBooks = customer.getBorrowedBooks();
        borrowedBooks.forEach(
                book -> System.out.println(borrowedBooks.indexOf(book) + ". " + book));
        String bookNumber = IO.readInput();
        if (isValidInput(bookNumber)) {
            return borrowedBooks.stream()
                    .filter(book -> book.equals(borrowedBooks.get(Integer.parseInt(bookNumber))))
                    .filter(book -> book.getLibrary().equals("Biblioteca"))
                    .findFirst()
                    .map(book -> {
                        customer.returnBook(book);
                        return CHECKIN_BOOK_MSG;
                    })
                    .orElse(WRONG_BOOK_MSG);
        } else {
            return WRONG_BOOK_MSG;
        }
    }

    private boolean isValidInput(String bookNumber) {
        return bookNumber.chars().allMatch(Character::isDigit) &&
                Integer.parseInt(bookNumber) >= 0 &&
                Integer.parseInt(bookNumber) < customer.getBorrowedBooks().size();
    }
}
