package com.twu.biblioteca.handler;

import com.twu.biblioteca.Customer;

public class InvalidOptionHandler implements OptionHandler {
    public static final String INVALID_OPTION_MSG = "Please select a valid option!";

    @Override
    public String handle(Customer customer) {
        return INVALID_OPTION_MSG;
    }
}
