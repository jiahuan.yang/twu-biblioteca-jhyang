package com.twu.biblioteca.handler;

import com.twu.biblioteca.Customer;

public interface OptionHandler {
    String handle(Customer customer);
}
