package com.twu.biblioteca.handler;

import com.twu.biblioteca.Biblioteca;
import com.twu.biblioteca.Customer;
import com.twu.biblioteca.IO;
import com.twu.biblioteca.Movie;

import java.util.List;

import static com.twu.biblioteca.IO.CHECKOUT_MOVIE_MSG;
import static com.twu.biblioteca.IO.MOVIE_NOT_AVAILABLE_MSG;

public class CheckoutMovieHandler implements OptionHandler {
    ;

    private Biblioteca biblioteca = Biblioteca.getBiblioteca();

    @Override
    public String handle(Customer customer) {
        System.out.println("Which movie would you like to check out?");
        List<Movie> availableMovies = biblioteca.getAvailableMovies();
        availableMovies.forEach(movie ->
                System.out.println(availableMovies.indexOf(movie) + ". " + movie));

        String movieNumber = IO.readInput();
        if (isValidInput(movieNumber)) {
            return availableMovies.stream()
                    .filter(movie -> movie.equals(availableMovies.get(Integer.parseInt(movieNumber))))
                    .findFirst()
                    .map(movie -> {
                        customer.borrowMovie(movie);
                        return CHECKOUT_MOVIE_MSG;
                    })
                    .orElse(MOVIE_NOT_AVAILABLE_MSG);
        } else {
            return MOVIE_NOT_AVAILABLE_MSG;
        }
    }

    private boolean isValidInput(String movieNumber) {
        return movieNumber.chars().allMatch(Character::isDigit) &&
                Integer.parseInt(movieNumber) >= 0 &&
                Integer.parseInt(movieNumber) < biblioteca.getAvailableMovies().size();
    }

}
