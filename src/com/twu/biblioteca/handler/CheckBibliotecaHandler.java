package com.twu.biblioteca.handler;

import com.twu.biblioteca.Biblioteca;
import com.twu.biblioteca.Customer;
import com.twu.biblioteca.IO;

import java.text.MessageFormat;

import static com.twu.biblioteca.IO.QUIT_MSG;

public class CheckBibliotecaHandler implements OptionHandler {
    private Biblioteca biblioteca = Biblioteca.getBiblioteca();

    @Override
    public String handle(Customer customer) {
        biblioteca.getAllBooks().forEach(book -> {
            if (book.owner().isEmpty()) {
                System.out.println(book.toString() + " is available.");
            } else {
                System.out.println(MessageFormat.format("{0} is borrowed by {1}", book.toString(), book.owner()));
            }
        });
        System.out.println("Press any key to exit Biblioteca.");
        IO.readInput();
        return QUIT_MSG;
    }
}
