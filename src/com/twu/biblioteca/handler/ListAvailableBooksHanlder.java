package com.twu.biblioteca.handler;

import com.twu.biblioteca.Biblioteca;
import com.twu.biblioteca.Customer;

public class ListAvailableBooksHanlder implements OptionHandler {
    private Biblioteca biblioteca = Biblioteca.getBiblioteca();

    @Override
    public String handle(Customer customer) {
        return biblioteca.getAvailableBooksAsString().stream()
                .map(book -> biblioteca.getAvailableBooksAsString().indexOf(book) + ". " + book)
                .reduce((a, b) -> a + "\n" + b)
                .orElse("");
    }
}
