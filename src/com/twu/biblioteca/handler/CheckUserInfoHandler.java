package com.twu.biblioteca.handler;

import com.twu.biblioteca.Customer;

import java.text.MessageFormat;

import static com.twu.biblioteca.IO.readInput;

public class CheckUserInfoHandler implements OptionHandler{
    @Override
    public String handle(Customer customer) {
        System.out.println(MessageFormat.format("name:{0}\nemail:{1}\nphone:{2}",
                customer.getName(), customer.getEmail(), customer.getPhone()));
        System.out.println("Please press any key to return to the main menu.");
        readInput();
        return "";
    }
}
