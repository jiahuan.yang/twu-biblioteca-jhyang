package com.twu.biblioteca.handler;

import com.twu.biblioteca.Customer;

import static com.twu.biblioteca.IO.QUIT_MSG;

public class QuitBibliotecaHandler implements OptionHandler {
    @Override
    public String handle(Customer customer) {
        return QUIT_MSG;
    }
}
