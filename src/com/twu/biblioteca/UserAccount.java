package com.twu.biblioteca;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.AbstractMap.SimpleEntry;

public class UserAccount {
    public static Map<String, String> userCredentialMap = Stream.of(
            new SimpleEntry<>("Jan", "123456"),
            new SimpleEntry<>("Annie", "123456"),
            new SimpleEntry<>("admin", "123456"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
}
