package com.twu.biblioteca;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Biblioteca {

    private List<Book> books;
    private List<Movie> movies;
    private static Biblioteca biblioteca = new Biblioteca();

    private Biblioteca() {
        books = new ArrayList<>();
        movies = new ArrayList<>();
        books.add(new Book("Refactoring", Collections.singletonList(new Author("Martin Fowler")), "1999", "Biblioteca"));
        books.add(new Book( "Enterprise Agility: Being Agile in a Changing World",
                Collections.singletonList(new Author("Sunil Mnudra")), "2018", "Biblioteca"));
        books.add(new Book("Building Evolutionary Architectures", Arrays.asList(new Author("Neal Ford"),
                new Author("Rebecca Parsons"), new Author("Patrick Kua")), "2017", "Biblioteca"));
        movies.add(new Movie("The Godfather", "1972", "Francis Ford Coppola"));
        movies.add(new Movie("The Dark Knight", "2008", "Christopher Nolan"));
        movies.add(new Movie("The Lord of the Rings: The Return of the King", "2003", "Peter Jackson"));
    }

    public static synchronized Biblioteca getBiblioteca() {
        if (biblioteca == null) {
            biblioteca = new Biblioteca();
        }
        return biblioteca;
    }

    public void destory () {
        biblioteca = null;
    }

    public List<Book> getAllBooks() {
        return books;
    }

    public List<String> getAllBooksAsString() {
        return books.stream()
                .map(Book::toString)
                .collect(Collectors.toList());
    }

    public List<String> getAllMoviesAsString() {
        return movies.stream()
                .map(Movie::toString)
                .collect(Collectors.toList());
    }

    public List<Book> getAvailableBooks() {
        return books.stream()
                .filter(book -> book.owner().isEmpty())
                .collect(Collectors.toList());
    }

    public List<Movie> getAvailableMovies() {
        return movies.stream()
                .filter(movie -> movie.owner().isEmpty())
                .collect(Collectors.toList());
    }

    public List<String> getAvailableBooksAsString() {
        return books.stream()
                .filter(book -> book.owner().isEmpty())
                .map(Book::toString)
                .collect(Collectors.toList());
    }

    public List<String> getAvailableMoviesAsString() {
        return movies.stream()
                .filter(movie -> movie.owner().isEmpty())
                .map(Movie::toString)
                .collect(Collectors.toList());
    }
}
