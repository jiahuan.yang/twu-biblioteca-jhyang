package com.twu.biblioteca;

import java.util.Scanner;

public class IO {
    public static final String WRONG_BOOK_MSG = "That is not a valid book to return.";
    public static final String CHECKOUT_BOOK_MSG = "Thank you! Enjoy the book";
    public static final String CHECKOUT_MOVIE_MSG = "Thank you! Enjoy the movie";
    public static final String BOOK_NOT_AVAILABLE_MSG = "Sorry, that book is not available";
    public static final String MOVIE_NOT_AVAILABLE_MSG = "Sorry, that movie is not available";
    public static final String CHECKIN_BOOK_MSG = "Thank you for returning the book";
    public static final String SUCCESSFUL_LOGIN_MSG = "Login successfully.";
    public static final String FAILED_LOGIN_MSG = "Wrong username or password.";
    public static final String QUIT_MSG = "Good Bye!";

    public static String readInput() {
        return new Scanner(System.in).next();
    }
}
