package com.twu.biblioteca;

import java.text.MessageFormat;

public class Movie implements HasOwner{
    private String name;
    private String year;
    private String director;
    private int rating;
    private String owner;

    public Movie(String name, String year, String director) {
        this.name = name;
        this.year = year;
        this.director = director;
        this.owner = "";
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} - {1} - {2}", name, director, year);
    }

    @Override
    public String owner() {
        return owner;
    }
}
