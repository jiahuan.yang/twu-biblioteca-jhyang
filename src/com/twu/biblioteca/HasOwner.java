package com.twu.biblioteca;

public interface HasOwner {
    String owner();
}
